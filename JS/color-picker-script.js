//color picker

// For testing purposes only
console.clear();

init_colorpicker_fn("#cp2", "rgb");

init_colorpicker_fn("#cp3", "hex");

function init_colorpicker_fn(id_str, format_str = "hex") {
  if (!id_str.startsWith("#")) {
    id_str = "#" + id_str;
  }

  var $picker_el = jQuery(id_str);

  $picker_el
    .colorpicker({
      format: format_str,
      horizontal: true,
      popover: {
        container: id_str + "-container",
      },
      template:
        '<div class="colorpicker">' +
        '<div class="colorpicker-saturation"><i class="colorpicker-guide"></i></div>' +
        '<div class="colorpicker-hue"><i class="colorpicker-guide"></i></div>' +
        '<div class="colorpicker-alpha">' +
        '	<div class="colorpicker-alpha-color"></div>' +
        '	<i class="colorpicker-guide"></i>' +
        "</div>" +
        '<div class="colorpicker-bar">' +
        '	<div class="input-group">' +
        '		<input class="form-control input-block color-io" />' +
        "	</div>" +
        "</div>" +
        "</div>",
    })
    /*.on("colorpickerCreate colorpickerUpdate", function (e) {
      $picker_el
        .parent()
        .find(".colorpicker-input-addon>i")
        .css("background-color", e.value);
    })*/
    .on("colorpickerCreate colorpickerUpdate", function (e) {
      $picker_el
        .parent()
        .find(".colorpicker-input-addon")
        .css("background-color", e.value);

      var chatHeaderBcColor = document.getElementsByClassName(
        "servicechat-header"
      );

      var chatToggleColor = document.getElementsByClassName(
        "servicechat-toggle"
      );

      var chatHeaderSFimgColor = document.getElementsByClassName(
        "serviceform-image"
      );

      var chatSendColor = document.getElementsByClassName(
        "servicechat-question-button"
      );

      var chatSendRocketColor = document.getElementsByTagName("polygon");

      var chatPopupMsgColor = document.getElementsByClassName(
        "sf-conversation"
      );

      for (i = 0; i < chatHeaderBcColor.length; i++) {
        chatHeaderBcColor[i].style.backgroundColor = e.value;
      }

      for (j = 0; j < chatToggleColor.length; j++) {
        chatToggleColor[j].style.backgroundColor = e.value;
      }

      for (k = 0; k < chatHeaderSFimgColor.length; k++) {
        chatHeaderSFimgColor[k].style.backgroundColor = e.value;
      }

      for (l = 0; l < chatSendColor.length; l++) {
        chatSendColor[l].style.backgroundColor = e.value;
      }

      for (m = 0; m < chatSendRocketColor.length; m++) {
        chatSendRocketColor[m].style.fill = e.value;
      }

      for (n = 0; n < chatPopupMsgColor.length; n++) {
        /* chatPopupMsgColor[n].setAttribute(
          "style",
          "color:" +
            e.value +
            " !important; border: 1px solid" +
            e.value +
            " !important"
        );*/

        var html = document.getElementsByTagName("html")[0];
        html.setAttribute(
          "style",
          "--main-popup-bg-color:" +
            e.value +
            "; --main-popup-font-color:" +
            e.value
        );
      }

      bgcolor = e.value;
    })
    .on("colorpickerCreate", function (e) {
      resize_color_picker_fn($picker_el);
    })
    .on("colorpickerShow", function (e) {
      var cpInput_el = e.colorpicker.popupHandler.popoverTip.find(".color-io");

      cpInput_el.val(e.color.string());

      cpInput_el.on("change keyup", function () {
        e.colorpicker.setValue(cpInput_el.val());
      });
    })
    .on("colorpickerHide", function (e) {
      var cpInput_el = e.colorpicker.popupHandler.popoverTip.find(".color-io");
      cpInput_el.off("change keyup");
    })
    .on("colorpickerChange", function (e) {
      var cpInput_el = e.colorpicker.popupHandler.popoverTip.find(".color-io");

      if (e.value === cpInput_el.val() || !e.color || !e.color.isValid()) {
        return;
      }

      cpInput_el.val(e.color.string());
    });

  /* $picker_el
    .parent()
    .find(".colorpicker-input-addon>i")
    .on("click", function (e) {
      $picker_el.colorpicker("colorpicker").show();
    });*/

  $picker_el
    .parent()
    .find(".colorpicker-input-addon")
    .on("click", function (e) {
      $picker_el.colorpicker("colorpicker").show();
    });

  jQuery(window).resize(function (e) {
    resize_color_picker_fn($picker_el);
  });
}

function resize_color_picker_fn($picker_el) {
  var rem_int = parseInt(getComputedStyle(document.documentElement).fontSize),
    width_int = $picker_el.parent().width() - rem_int * 0.75 * 2 - 2,
    colorPicker_obj = $picker_el.colorpicker("colorpicker");
}

var chatHeaderBcColor = document.getElementsByClassName("servicechat-header");
